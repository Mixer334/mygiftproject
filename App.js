import { createAppContainer } from 'react-navigation';
//Opted for a Stack Navigation
import { createStackNavigator } from 'react-navigation-stack';

import React from 'react';
//Screens
import HomeScreen from './src/screens/HomeScreen';
import AddEditGiftScreen from './src/screens/AddEditGiftScreen';
import ContactListScreen from './src/screens/ContactListScreen';

//Gift context provider, that way it can provide the context to any screen
import { Provider } from './src/context/GiftContext';


const navigator = createStackNavigator({
  Home: HomeScreen,
  AddEditGift: AddEditGiftScreen,
  ContactList: ContactListScreen,
}, {
  initialRouteName: 'Home',
  defaultNavigationOptions: {
    headerTintColor: 'white',
    headerStyle: {
      backgroundColor: '#FEB3FF',
    },
    title: 'myGift'
  },
});
const App = createAppContainer(navigator);

export default () => {
  return (
    <Provider>
    <App />
    </Provider>
    );
};