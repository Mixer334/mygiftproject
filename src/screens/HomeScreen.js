import React, {useContext, useState} from 'react';
import {View, StyleSheet, Text, FlatList, Dimensions, TouchableOpacity, Image, Modal} from 'react-native';
import { AntDesign,Ionicons } from '@expo/vector-icons';
import ModalDelete from '../component/ModalDelete';
import { Context } from '../context/GiftContext';
//Home screen. It contains every gift idea created by the user.
//Select gift idea to edit, select delete icon to delete
//Uses context in order to observe the state of gift and uses the deleteGift in order to delete gift.
const { width, height } = Dimensions.get('window');
const SCREEN_WIDTH = width < height ? width : height;
const HomeScreen = ({navigation}) => {
	const [modalOpen, setModalOpen] = useState(false);
	const [idSelected, setSelected] = useState('');
	const {state, deleteGift} = useContext(Context);

	return (
		<View style={{flex:1}}> 

			<ModalDelete
				modalOpen={modalOpen}
				closeModal={() => setModalOpen(false)}
				confirmDelete={() => deleteGift(idSelected)}
			/>

			
			<FlatList 
				columnWrapperStyle={{justifyContent: 'center'}}
				numColumns={2}
				horizontal={false}
				data={state}
				keyExtractor={(gift) => gift.title}
				ListEmptyComponent={() => (
					<View style={styles.noItems}>
						<Text style={{fontSize:20, marginBottom:10}}> No Gift Ideas made yet! </Text>
						<Text style={{fontSize:13}}> Start by selecting the add icon to create a gift idea </Text>
					</View>
				)}
				renderItem={({item}) => {
					return (
						<TouchableOpacity onPress={ () => navigation.navigate('AddEditGift', {id: item.id})}>
						<View style={styles.shadow}>
							<View style={styles.row}>
								<Text numberOfLines={1} style={styles.title}>{item.title}</Text>
								{item.imageUri && <Image source={{ uri: item.imageUri }} resizeMode='contain' style={{ width: 150, height: 150 }} />}
								{!item.imageUri && <Image source={require('../../assets/Gift2.png')} style={{ width: 150, height: 150 }} />}
								<View style={{flexDirection:'row', marginTop:10, marginBottom:5, borderTopWidth:1}}>

									<View style={{alignSelf:'flex-start', flex:1.2}}>
										<Text>Contact:</Text>
										<Text style={{fontSize:12}}>{item.contact}</Text>
									</View>
									<TouchableOpacity style={{justifyContent:'center'}} onPress={() => {
										setModalOpen(true);
										setSelected(item.id);
									}}
									>
									<View style={{alignSelf:'center', flex:0.2, justifyContent:'center',borderLeftWidth:1, paddingLeft:10, paddingRight:5}}>
										
										<AntDesign name="delete" size={20} color="red" />

									</View>
									</TouchableOpacity>
								</View>
							</View>
						</View>
						</TouchableOpacity>
					);
				}}
			/>
		</View>
	);
};

HomeScreen.navigationOptions = ({navigation}) => {
	return {
		headerRight: () => (
			<TouchableOpacity onPress={() => navigation.navigate('AddEditGift')}>
				<AntDesign name="pluscircle" style={styles.headerIcon} />
			</TouchableOpacity>
		),
		headerLeft: () => (
			<Ionicons name="md-gift-sharp" style={styles.headerIconLeft} size={24} color="white" />
		),
	};
};

const styles = StyleSheet.create({
	 
	headerIcon: {
		fontSize: 24,
		marginRight:15,
		color:'white'
	},
	headerIconLeft: {
		fontSize:24,
		color:'white',
		marginLeft:20,
		paddingRight:-10,

	},
	shadow:{
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 1,
		},
		shadowOpacity: 0.4,
		shadowRadius: 2,
		elevation: 2,
		backgroundColor : 'transparent',
		margin:5,
		borderWidth:0,
		borderRadius: 2,
		
	},
	row: {
		alignItems: 'center',
		paddingTop: 15,
		paddingHorizontal: 10,
		flex:0.5,
		minHeight:260,
		width:(SCREEN_WIDTH/2.1)
	},
	title:{
		fontSize: 18,
		marginBottom:5,
	},
	noItems:{
		flex:1,
		alignItems:'center',
		justifyContent:'center',
		marginTop:100,
		marginRight:30,
		marginLeft:30
	}
});

export default HomeScreen;