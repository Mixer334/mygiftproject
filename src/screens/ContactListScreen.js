import React, {useEffect, useState} from 'react';
import {View, StyleSheet, Text, FlatList, TouchableOpacity} from 'react-native';
import * as Contacts from 'expo-contacts';
import { MaterialIcons } from '@expo/vector-icons';
import SearchContact from '../component/SearchContact';
//Contact list screen, uses expo-contact in order to get the contacts in your phone
//Can't take cell phone numbers
const ContactListScreen = ({navigation}) => {
  const [term,setTerm] = useState('');
	const [contacts, setContacts] = useState([]);
  const [contactsMemory, setContactsMem] = useState([]);
	useEffect(() => {
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === 'granted') {
        const { data } = await Contacts.getContactsAsync({
          fields: [Contacts.Fields.phoneNumbers]
        });

        if (data.length > 0) {
          setContacts(data);
          setContactsMem(data);
        }
      }
    })();
  }, []);

  const searchContact = (term) => {
    const filteredContact = contactsMemory.filter(contact => {
      let contactLowerCase = (contact.firstName + ' ' + contact.lastName).toLowerCase();

      let searchTermLowerCase = term.toLowerCase();

      return contactLowerCase.indexOf(searchTermLowerCase) > -1;
    });

    setContacts(filteredContact);
  }

  const callback = navigation.getParam('callback');
	return (
    <View style={{flex:1}}>
      <SearchContact
        term={term}
        onTermChange={newValue => {
          setTerm(newValue);
          searchContact(newValue);

        }}
      />

    	<FlatList
    		data={contacts}
    		keyExtractor = {(contact) => contact.id}
        ListEmptyComponent={() => (
          <View style={styles.noItems}>
            <Text style={{fontSize:20, marginBottom:10}}> No contacts found! </Text>
          </View>
        )}
    		renderItem={({item}) => {
    			return (
          <TouchableOpacity onPress={() => {
            callback(item.name);
            navigation.navigate('AddEditGift');
            }}
          >
    				<View style={styles.listContact}>
    					<MaterialIcons name="contact-page" style={styles.icon}/>
    					<Text style={styles.nomeContact}>{item.name}</Text>
    				</View>
          </TouchableOpacity>
    			);
    		}}
    	/>
    </View>
  );
};

const styles = StyleSheet.create({
	listContact: {
		flexDirection :'row',
		paddingVertical: 20,
		minHeight:70,
		padding: 5,
		borderBottomWidth: 0.3,
		borderColor: 'grey',
		paddingHorizontal: 10
	},
	nomeContact: {
		fontSize:16,
		fontWeight: 'bold',
		paddingHorizontal: 15,
		alignSelf: 'center'
	},
	icon:{
		fontSize:24,
		color:'black',
		alignSelf: 'center'
	},
  noItems:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    marginTop:100,
    marginRight:30,
    marginLeft:30
  }
});

export default ContactListScreen;