import React, {useContext, useState,  useEffect} from 'react';
import {View, Text,TextInput, StyleSheet, Button, Platform, Image, TouchableOpacity} from 'react-native';
import {Context} from '../context/GiftContext';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import { AntDesign, Entypo, EvilIcons, Ionicons} from '@expo/vector-icons';
import ModalIdea from '../component/ModalIdea';
//Screen to Add and Edit gift ideas
//If (id) is in params of navigation, it is presumed as edit
const AddEditGiftScreen = ({navigation}) => {
	const {state, addGift, editGift} = useContext(Context);
	const [image, setImage] = useState(null);
	const [title, setTitle] = useState ('');
	const [contact, setContact] = useState('');
	const [errorMessage, setError] = useState('');
	const [modalOpen, setModalOpen] = useState(false);

	useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();

    if (navigation.getParam('id')){
    	const id = navigation.getParam('id');

    	const giftIdea= state.find(
			(giftIdea) => giftIdea.id === id
		);
		setImage(giftIdea.imageUri);
		setTitle(giftIdea.title);
		setContact(giftIdea.contact);
		navigation.setParams({ titlePage: 'Edit Gift' });
    }

	}, []);

	const pickImage = async () => {
	    let result = await ImagePicker.launchImageLibraryAsync({
	      mediaTypes: ImagePicker.MediaTypeOptions.All,
	      allowsEditing: true,
	      aspect: [4, 3],
	      quality: 1,
	    });

	    

	    if (!result.cancelled) {
	      setImage(result.uri);
	    }
	};


	const onSelect = (data) => {
		setContact(data);
	}

	const finishButton = () => {
		if(!title || !contact) {
			setError('Please fill every field!');
		} else {
			if (!navigation.getParam('id')) {
				addGift(title,image,contact, () => navigation.navigate('Home'));
			} else {
				editGift(navigation.getParam('id'),title,image,contact, () => navigation.pop());
			}
		}
	}

	return (
		<View style={styles.Container}>

			<ModalIdea 
				modalOpen={modalOpen}
				closeModal={() => setModalOpen(false)}
				chooseIdea={(imageTeste,titleTeste) => {
					setImage(imageTeste);
					setTitle(titleTeste);
				}}
			/> 

			<View style={styles.UpperCon}>
				<View style={{backgroundColor:'#28D6C0', borderTopLeftRadius:30,borderTopRightRadius:30, width:160, height:48, marginLeft:20, marginTop:15, justifyContent:'center'}}>
				<Text style={styles.label}> Enter Gift Title: </Text>
				</View>
				<TextInput style={styles.input} value={title} onChangeText={text => setTitle(text)}/>

			    <TouchableOpacity onPress={pickImage}>

			    	<View style={styles.giftImage}>
			    		
				    	{!image ?
				    	 <Image source={require('../../assets/Gift2.png')} style={{width:200,height:200}} />
				    	 :
				    	 <Image source={{ uri: image }} resizeMode='contain' style={{width:200,height:200}} />
				    	}
				    	{!image && <EvilIcons name="plus" style={styles.iconPlus}/>}
			    	</View>
			    </TouchableOpacity>

			    <TouchableOpacity onPress={() => setModalOpen(true)}>
			    	<View style={styles.giftButton}>
			    		<Ionicons name="md-gift-sharp" size={24} color="grey" />
			    		<Text style={styles.linkText}> Gift Ideas </Text>
			    	</View>
			    </TouchableOpacity>

			    <TouchableOpacity 
			    	onPress={(contact) => 
			    		navigation.navigate('ContactList', {callback: onSelect})
			    	} 
			    >

			    
			    <Text style={{fontSize:18, fontWeight:'bold', color:'#28D6C0', paddingLeft:35}}> Pick a contact : </Text>
			    
			    <View style={styles.contactBox}>
			    	<AntDesign style={{position:'absolute',paddingLeft:5}}name="contacts" size={24} color="black" />
			    	<Text style={{fontWeight:'bold',flex:1, textAlign:'center'}}> {contact} </Text>
			    </View>
			    </TouchableOpacity>
		    </View>

		    <View style={styles.BottomCon}>
			    <Text style={{color:'red', textAlign:'center', marginBottom: 5}}> {errorMessage} </Text>
			    <TouchableOpacity onPress={finishButton}>
			    	<View style={styles.saveButton}>
			    		<Text style={{fontWeight:'bold', fontSize:24, color:'white'}}> Save </Text>
			    	</View>
			    </TouchableOpacity>
		   	</View>
		</View>
	);
};

const styles = StyleSheet.create({
	input:{
		fontSize: 18,
		borderWidth: 2,
		borderColor: '#28D6C0',
		marginBottom: 15,
		paddingLeft:10,
		padding: 5,
		margin: 5,
		borderRadius: 15,
		backgroundColor: 'white',

	},
	label:{
		fontSize:20,
		marginBottom:5,
		marginTop:10,
		fontWeight:'bold',
		color:'white',
		textAlign:'center'
	},
	giftImage:{
		backgroundColor:'white',
		width: 220, 
		height: 220,
		alignSelf: 'center',
		justifyContent: 'center',
		alignItems:'center',
		marginBottom: 5,
	},
	contactBox: {
		flexDirection:'row',
		borderWidth:2,
		borderColor:'#28D6C0',
		borderRadius: 30,
		marginHorizontal: 7,
		marginVertical: 5,
		marginBottom: 15,
		height: 40,
		width:300,
		alignSelf:'center',
		alignItems: 'center',
		backgroundColor: 'white',
	},
	saveButton: {
		borderWidth:5,
		borderColor:'#28D6C0',
		backgroundColor: '#28D6C0',
		borderRadius:30,
		height:50,
		width:100,
		alignSelf:'center',
		justifyContent:'center',
		alignItems:'center'
	},
	Container:{
		flex:1,
		backgroundColor:'white'
	},
	
	BottomCon:{
		
		justifyContent:'center'
	},
	iconPlus:{
		color:'black',
		fontSize:30,
		position: 'absolute',
		right:15,
		bottom:0,
		opacity:0.5

	},
	giftButton:{
		flexDirection:'row',
		borderWidth:0,
		borderColor:'#d1cfd2',
		backgroundColor: '#d1cfd2',
		borderRadius:30,
		height:40,
		width:150,
		alignSelf:'center',
		justifyContent:'center',
		alignItems:'center',
		marginBottom:25
	},
	linkText: {
		color:'white',
		alignSelf:'center',
		fontWeight:'bold'
	}
});

AddEditGiftScreen.navigationOptions = ({navigation}) => {
	return {
		title: navigation.getParam('titlePage', 'Add a new gift'),
	};
};

export default AddEditGiftScreen;