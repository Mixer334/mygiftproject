import React, {useReducer} from 'react';
import createDataContext from './createDataContext';
//Gift context (Add gift, delete gift , edit gift)
//Gift : Title of gift, gift image url, contact name
const giftReducer = (state, action) => {
	switch (action.type) {
		case 'add_gift':
			return [...state, 
				{ 
					id: Math.floor(Math.random() * 99999), 
					title: action.payload.title,
					imageUri: action.payload.imageUri,
					contact: action.payload.contact
				},
			];
		case 'edit_gift':
			return state.map((giftIdea) => {
				return giftIdea.id ===action.payload.id ? action.payload : giftIdea
			});
		case 'delete_gift':
			return state.filter((giftIdea) => giftIdea.id !== action.payload);
		default :
			return state;
	}
};

const addGift = (dispatch) => {
	return (title, imageUri, contact, callback) => {
		dispatch({type: 'add_gift', payload: {title, imageUri, contact}});

		if (callback) {
			callback();
		}
	};
};

const editGift = (dispatch) => {
	return (id, title, imageUri, contact, callback) => {
		dispatch({type: 'edit_gift', payload: {id, title, imageUri, contact}});

		if (callback) {
			callback();
		}
	};
};

const deleteGift = (dispatch) => {
	return (id) => {
		dispatch({type: 'delete_gift', payload: id});
	};
};

export const {Context, Provider} = createDataContext(
	giftReducer, 
	{addGift, editGift, deleteGift}, 
	[{title: 'TEST', id: 1, contact:'TEST CONTACT'}]
);