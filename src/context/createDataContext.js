import React, {useReducer} from 'react';
//Automating creating context, only usefull if in the future ill add another context but for now only giftIdea
export default (reducer, actions, initialState) => {
	const Context = React.createContext();

	const Provider = ({children}) => {
		const [state, dispatch] = useReducer(reducer, initialState);

		// actions === Exemplo-> {addBlogPost: (dispatch) => { return () => {} } }
		const boundAction = {};
		for (let key in actions) {
			boundAction[key] = actions[key](dispatch);
		}

		return (
			<Context.Provider value={{ state, ...boundAction }}>
				{children}
			</Context.Provider>
		);
	};

	return {Context, Provider};
};