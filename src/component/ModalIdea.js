import React, {useState} from 'react';
import {Text, StyleSheet, Modal, View, TouchableOpacity, FlatList} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import useResults from '../hooks/useResults';
import ResultDetail from './ResultDetail';
//Modal that contains the products from ebay api, Screen : AddEditGiftIdea
const ModalIdea = ({modalOpen, closeModal, chooseIdea}) => {

	const [searchApi, results, errorMessage] = useResults();
	

	return(
		<View>
			<Modal visible={modalOpen} animationType='slide'>
		        <View style={styles.modalContent}>
			        <MaterialIcons 
			          name='close'
			          size={24} 
			          style={{...styles.modalToggle, ...styles.modalClose}} 
			          onPress={() => closeModal()} 
			        />
			        {errorMessage ? <Text style={{alignSelf:'center'}}> {errorMessage} </Text> : null}
					<FlatList
						data={results}
						keyExtractor={(result) => result.epid}
						renderItem={({item}) => {
							return (
								<TouchableOpacity onPress={() => {
									chooseIdea(item.image.imageUrl,item.title);
									closeModal();
								}}>
									<ResultDetail result = {item} />
								</TouchableOpacity>
							);
						}}
					/>
		        </View>
	      	</Modal>	
		</View>
	);
};

const styles = StyleSheet.create({
	modalToggle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
    borderWidth: 1,
    borderColor: '#f2f2f2',
    padding: 10,
    borderRadius: 10,
    alignSelf: 'center',
    marginBottom:10
  },
  modalClose: {
    marginTop: 20,
    marginBottom: 20,
    backgroundColor:'grey',
    color:'white'
  },
  modalContent: {
    flex: 1,
  }
});

export default ModalIdea;