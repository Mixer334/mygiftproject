import React, {useState} from 'react';
import {Text, StyleSheet, Modal, View, TouchableOpacity} from 'react-native';
//Modal that confirms if the user whishes to delete the gift idea
const ModalDelete = ({modalOpen, closeModal, confirmDelete}) => {
	return(
		<View>
			<Modal transparent={true} visible={modalOpen} animationType='fade'>
		        <View style={styles.modalContent}>
		        	
		        	<View opacity={1} style={styles.modalBox}>
			        <Text style={{fontWeight:'bold', fontSize:15, color:'white'}}>Are you sure you want to delete this gift idea?</Text>
			        <View style={{flexDirection:'row'}}>
			        <View style={{marginRight:20}}>
			          <TouchableOpacity 
			            style={{...styles.modalToggle, ...styles.modalClose}} 
			            onPress={() => {
			            	closeModal(false);
			            	confirmDelete();
			            }} 
			          >
			          <Text style={{color:'white',fontWeight:'bold'}}> Yes </Text>
			          </TouchableOpacity>
			        </View>
			          <TouchableOpacity 
			            style={{...styles.modalToggle, ...styles.modalClose}} 
			            onPress={() => closeModal()} 
			          >
			          <Text style={{color:'white', fontWeight:'bold'}}> Cancel </Text>
			          </TouchableOpacity>
			          </View>
			        </View>
		        </View>
		    </Modal>	
		</View>
	);
}

const styles = StyleSheet.create({
	modalToggle: {
	    justifyContent: 'center',
	    alignItems: 'center',
	    marginBottom: 10,
	    borderWidth: 1,
	    borderColor: '#f2f2f2',
	    padding: 10,
	    borderRadius: 10,
	    alignSelf: 'center',

	},
	modalClose: {
	    marginTop: 20,
	    marginBottom: 0,
	},
	modalContent: {
	    flex:1,
	    justifyContent:'center',
	    alignItems:'center',
	    backgroundColor:'#00000070'
	},
	modalBox:{
		backgroundColor:'#28D6C0',
		justifyContent: 'center',
	    alignItems: 'center',
	    marginBottom: 10,
	    borderWidth: 1,
	    borderColor: '#f2f2f2',
	    padding: 10,
	    borderRadius: 10,
	    alignSelf: 'center',
	},
});

export default ModalDelete;