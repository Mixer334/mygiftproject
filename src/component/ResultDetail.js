import React from 'react';
import {View, Text , StyleSheet, Image} from 'react-native';
//Used for Ebay api, title and image of the BEST SELLING products
const ResultDetail = ({result}) => {
	return (
		<View style={styles.container}>
			<Image style={styles.image} resizeMode='contain' source={{uri: result.image.imageUrl}} />
			<Text style={styles.name}> {result.title} </Text>
		</View>
	);
}

const styles = StyleSheet.create({
	container: {
		
		marginTop:15,
		marginBottom:10,
		flex:1,
		alignItems: 'center',
		borderBottomWidth: 1,
		borderColor: 'grey'
	},
	image: {
		minWidth: 250,
		minHeight:300,
		borderRadius: 4
	},
	name: {
		fontWeight: 'bold',
		fontSize: 16,
		paddingHorizontal:35,
		paddingBottom:10,
		textAlign:'center'
	}
});

export default ResultDetail;
