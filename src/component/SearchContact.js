import React from 'react';
import {View , TextInput , StyleSheet } from 'react-native';
import { Feather } from '@expo/vector-icons';
//Search bar for ContactListScreen
const SearchContact = ({term, onTermChange}) => {

	return (
		<View style={styles.background}>
			<Feather name="search" style={styles.iconStyle}/>
			<TextInput 
				autoCapitalize="none"
				autoCorrect={false}
				style = {styles.inputStyle}
				placeholder= "Search"
				value={term}
				//onChangeText={newTerm => onTermChange(newTerm)}
				onChangeText={onTermChange}
			/> 
		</View>
	);
};

const styles = StyleSheet.create({
	background: {
		marginTop:10,
		backgroundColor: '#d4d4d4',
		height: 50,
		borderRadius: 5,
		marginHorizontal: 15,
		flexDirection: 'row',
		marginBottom: 10
	},
	inputStyle: {
		flex:1,
		fontSize:18
	},
	iconStyle: {
		fontSize: 35,
		alignSelf: 'center',
		marginHorizontal: 13
	}
});

export default SearchContact;