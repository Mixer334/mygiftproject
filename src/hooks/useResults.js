import React, {useEffect, useState} from 'react';
import ebay from '../api/ebay';
//Hook to search for which category (BEST SELLING)
//Category id must be 9355 to work in sandbox mode
export default () => {
	const [results, setResults] = useState([]);
	const [errorMessage, setErrorMessage] = useState('');

	//Importante, para buscar info api
	const searchApi = async (searchTerm) => {
		try {
		const response = await ebay.get('/merchandised_product', {
			params: {
				metric_name: searchTerm,
				category_id: 9355,
			}
		});
		setResults(response.data.merchandisedProducts);
		} catch (err) {
			console.log(err);
			setErrorMessage('Something went Wrong');
		}
	};

	useEffect (() => {
		searchApi('BEST_SELLING');
	}, []);

	return [searchApi,results, errorMessage];
};